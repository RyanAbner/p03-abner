//
//  ViewController.m
//  p03-abner
//
//  Created by Ryan Abner on 2/23/17.
//  Copyright © 2017 Ryan Abner. All rights reserved.


#import "ViewController.h"
#import "BaseScene.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    SKView * spriteView = (SKView *) self.view;
    spriteView.showsDrawCount = YES;
    spriteView.showsNodeCount = YES;
    spriteView.showsFPS = YES;
}

- (void)viewWillAppear:(BOOL)animated {
    BaseScene *base = [[BaseScene alloc] initWithSize:CGSizeMake(BASE_WIDTH,BASE_HEIGHT)];
    SKView *spriteView = (SKView *) self.view;
    [spriteView presentScene:base];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
