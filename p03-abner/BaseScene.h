//
//  BaseScene.h
//  p03-abner
//
//  Created by Ryan Abner on 2/23/17.
//  Copyright © 2017 Ryan Abner. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "GameOverScene.h"
#define BASE_WIDTH 768
#define BASE_HEIGHT 10240
@interface BaseScene : SKScene
//@property int maxHeight;

@end
