//
//  BaseScene.m
//  p03-abner
//
//  Created by Ryan Abner on 2/23/17.
//  Copyright © 2017 Ryan Abner. All rights reserved.
//  Parts of this code are based off of Apple's Spritekit Programming Guide.

/*
Player physics:
Contact with bottom of platform: go through
Contact with top of platform: apply upward impulse
Contact with left edge: teleport to right edge
Contact with right edge: teleport to left edge
Contact with ground for first time: apply upward impulse
Contact with ground after that: game over
Contact with bottom of screen: game over

Press left button: Add left force
Press right button: add right force
Reach middle of screen: scroll up, increase score (platforms should be generated)

 
Platform is two nodes, a bottom and a top one.
*/


#import "BaseScene.h"

@interface BaseScene()
@property BOOL contentCreated;
@property SKSpriteNode *player;
@property SKSpriteNode *ground;
@property SKSpriteNode *camParent;
@property SKSpriteNode *leftButton;
@property SKSpriteNode *rightButton;
@property SKSpriteNode *leftSide;
@property SKSpriteNode *rightSide;
@property SKSpriteNode *ballParent;
@property SKTexture *ballTexture;
@property BOOL groundHasBeenHit;
@property CGVector jumpVector;
@property SKLabelNode *scoreLabel;
@property SKSpriteNode *gameOverNode;
@end
static const uint32_t playerCategory = 0x1 << 0;
static const uint32_t groundCategory = 0x1 << 1;
static const uint32_t ballCategory = 0x1 << 2;
static const uint32_t obstacleCategory = groundCategory | ballCategory;
static const uint32_t leftEdgeCategory = 0x1 << 4;
static const uint32_t rightEdgeCategory = 0x1 << 5;
static const uint32_t bottomEdgeCategory = 0x1 << 6;
static const uint32_t topEdgeCategory = 0x1 << 7;
static const uint32_t sideEdgeCategory = leftEdgeCategory | rightEdgeCategory;
static const uint32_t edgeCategory = sideEdgeCategory | bottomEdgeCategory | topEdgeCategory;

static const int JUMP_POWER = 510;
static const int STAR_JUMP_POWER = 400;
static const int PUSH_POWER = 75;
int maxHeight;
bool gameOver;

@implementation BaseScene
- (void)didMoveToView:(SKView *)view {
    if (!self.contentCreated){
        [self createSceneContents];
    }
}


//Generates a star with a random X and specific Y
- (SKSpriteNode*)generateStar:(int) yPos{
    SKSpriteNode *star = [SKSpriteNode spriteNodeWithTexture:_ballTexture];
    int xPos = arc4random_uniform(768);
    [star setScale:1.75];
    star.position = CGPointMake(xPos,yPos);
    star.physicsBody = [SKPhysicsBody bodyWithTexture:_ballTexture alphaThreshold:0.1 size:star.size];
    star.physicsBody.dynamic = NO;
    star.physicsBody.collisionBitMask = 0;
    star.physicsBody.categoryBitMask = ballCategory;
    star.physicsBody.contactTestBitMask = playerCategory;
    
    return star;
}

//They're actually stars.
- (void)generateBalls {
    //generate or don't generate a node with X% chance every Y units, starting from Z units above ground
    int whereToPlace = 100;//first spot to place a node
    int increment = 150;
    [self addChild:[self generateStar:whereToPlace]];
    for (whereToPlace = 110; whereToPlace < 10240; whereToPlace += increment){
        if (arc4random_uniform(100) < 75){
            [self addChild:[self generateStar:whereToPlace]];
        }
    }
}

- (void)createSceneContents {
    self.physicsWorld.contactDelegate = self;
    maxHeight = 0;
    gameOver = false;

    self.jumpVector = CGVectorMake(0,JUMP_POWER);
    //self.scaleMode = SKSceneScaleModeAspectFit;
    self.backgroundColor = [SKColor cyanColor];
    //Create ground sprite
    _ground = [SKSpriteNode spriteNodeWithImageNamed:@"grassTiled.png"];
    _ground.position = CGPointMake(BASE_WIDTH/2,35);
    _ground.name = @"ground";
    _ground.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:_ground.size];
    _ground.physicsBody.dynamic = NO;
    _ground.physicsBody.categoryBitMask = groundCategory;
    _ground.physicsBody.contactTestBitMask = playerCategory;
    _ground.physicsBody.collisionBitMask = 0;
    
    _groundHasBeenHit = NO;
    [self addChild: _ground];
    
    
    //Create player
    _player = [SKSpriteNode spriteNodeWithImageNamed:@"p1_jump.png"];
    _player.position = CGPointMake(BASE_WIDTH/2,512);
    _player.name = @"player";
    _player.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:_player.size];
    _player.physicsBody.categoryBitMask = playerCategory;
    _player.physicsBody.collisionBitMask = 0;
    _player.physicsBody.contactTestBitMask = obstacleCategory | edgeCategory | ballCategory;
    [self addChild: _player];
    
    //Create camera
    SKCameraNode *camera = [SKCameraNode node];
    self.camera = camera;
    camera.position = CGPointMake(0,0);
    camera.yScale = 0.1;
    
    _camParent = [SKSpriteNode node];
    _camParent.position = CGPointMake(768/2,1024/2);
    [_camParent addChild:camera];
    SKSpriteNode *bottomNode = [SKSpriteNode node];
    //The bottomNode moves with the camera and will always be at the bottom of the screen.
    bottomNode.physicsBody = [SKPhysicsBody bodyWithEdgeFromPoint:CGPointMake(-512,-560)
                                                          toPoint:CGPointMake(10000,-560)];
    bottomNode.physicsBody.collisionBitMask = 0;
    bottomNode.physicsBody.categoryBitMask = bottomEdgeCategory;
    bottomNode.physicsBody.contactTestBitMask = playerCategory;
    [_camParent addChild:bottomNode];
    [self addChild:_camParent];
    
    //Create left and right buttons - these are a child of _camParent so they move with it
    _leftButton = [SKSpriteNode spriteNodeWithImageNamed:@"signLeft.png"];
    [_camParent addChild:_leftButton];
    _leftButton.position = CGPointMake(-290,-392);
    _leftButton.name = @"leftButton";
    [_leftButton setScale:1.5];
    [_leftButton setZPosition:4];
    
    _rightButton = [SKSpriteNode spriteNodeWithImageNamed:@"signRight.png"];
    [_camParent addChild:_rightButton];
    _rightButton.position = CGPointMake(-190,-392);
    _rightButton.name = @"rightButton";
    [_rightButton setScale:1.5];
    [_rightButton setZPosition:4];
    
    _scoreLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
    _scoreLabel.position = CGPointMake(300,450);
    [_scoreLabel setScale:.75];
    [_camParent addChild:_scoreLabel];
    
    //Set up side edge nodes to teleport player to other side
    //Create side nodes, set up like bottom node, then set what they do in the collision function
    _leftSide = [SKSpriteNode node];
    _rightSide = [SKSpriteNode node];
    [self addChild:_leftSide];
    [self addChild:_rightSide];
    _leftSide.physicsBody = [SKPhysicsBody bodyWithEdgeFromPoint:CGPointMake(0,0) toPoint:CGPointMake(0,10240)];
    _rightSide.physicsBody = [SKPhysicsBody bodyWithEdgeFromPoint:CGPointMake(BASE_WIDTH,0) toPoint:CGPointMake(BASE_WIDTH,10240)];
    _leftSide.physicsBody.collisionBitMask = 0;
    _rightSide.physicsBody.collisionBitMask = 0;
    _leftSide.physicsBody.categoryBitMask = leftEdgeCategory;
    _rightSide.physicsBody.categoryBitMask = rightEdgeCategory;
    _leftSide.physicsBody.contactTestBitMask = playerCategory;
    _rightSide.physicsBody.contactTestBitMask = playerCategory;
    
    //Generate stars
    _ballTexture = [SKTexture textureWithImageNamed:@"star.png"];
    _ballParent = [SKSpriteNode node];
    [self addChild:_ballParent];
    [self generateBalls];
    
    //Create gameOverNode that displays game over message, score, and restart button
    _gameOverNode = [SKSpriteNode node];
    [_gameOverNode setSize:CGSizeMake(300,200)];
    [_gameOverNode setColor:[SKColor blackColor]];
    [_gameOverNode setZPosition:5];
    
    
    
    
    
    self.contentCreated = YES;
}

- (void)update:(NSTimeInterval)currentTime {
    if (_player.position.y  > maxHeight){
        maxHeight = _player.position.y;
        [_camParent runAction:[SKAction moveToY:maxHeight duration:0]];
    }
    NSString *scoreString = [NSString stringWithFormat:@"Score: %d",maxHeight-512];
    [_scoreLabel setText:scoreString];
    
    
    if (maxHeight-512 > 10000 && !gameOver){
        NSString *scoreString = [NSString stringWithFormat:@"Your score is %d.",maxHeight-512];
        SKLabelNode *finalScoreLabel = [SKLabelNode labelNodeWithText:scoreString];
        [finalScoreLabel setFontName:@"Futura"];
        [finalScoreLabel setFontSize:20];
        [finalScoreLabel setFontColor:[SKColor yellowColor]];
        [finalScoreLabel setPosition:CGPointMake(0,50)];
        
        SKLabelNode *youWinLabel = [SKLabelNode labelNodeWithText:@"That's over 10,000, so you win!"];
        [youWinLabel setFontName:@"Futura"];
        [youWinLabel setFontSize:20];
        [youWinLabel setFontColor:[SKColor yellowColor]];
        [youWinLabel setPosition:CGPointMake(0,0)];
        
        
        SKLabelNode *tryAgainLabel = [SKLabelNode labelNodeWithText:@"Tap anywhere to play again!"];
        [tryAgainLabel setFontName:@"Futura"];
        [tryAgainLabel setFontSize:20];
        [tryAgainLabel setFontColor:[SKColor whiteColor]];
        [tryAgainLabel setPosition:CGPointMake(0,-50)];
        
        [_gameOverNode addChild:tryAgainLabel];
        [_gameOverNode addChild:youWinLabel];
        [_gameOverNode addChild:finalScoreLabel];
        [_gameOverNode setColor:[SKColor greenColor]];
        [_camParent addChild:_gameOverNode];
        [self removeChildrenInArray:@[_player]];
        
        gameOver =true;
    }
}


//Partially based on this:
//http://stackoverflow.com/questions/23330133/perform-action-when-sprite-is-touched
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    if (!self.contentCreated) return;

    UITouch *touch = [touches anyObject];
    CGPoint touchLocation = [touch locationInNode:self.scene];
    
    //If it's a directional button, apply impulse
    if (CGRectContainsPoint(_leftButton.frame,[self convertPoint:touchLocation toNode:_camParent])){
        [_player.physicsBody applyImpulse:CGVectorMake(-PUSH_POWER,0)];
    } else if (CGRectContainsPoint(_rightButton.frame,[self convertPoint:touchLocation toNode:_camParent])){
        [_player.physicsBody applyImpulse:CGVectorMake(PUSH_POWER,0)];
    }
    
    if (gameOver){
        SKView *parentView = self.view;
        BaseScene *base = [[BaseScene alloc] initWithSize:CGSizeMake(BASE_WIDTH,BASE_HEIGHT)];
        [parentView presentScene:base];
    }
}

- (void)didBeginContact:(SKPhysicsContact *) contact {
    SKPhysicsBody *secondBody;//the body that isn't the player
    if (contact.bodyA == _player.physicsBody){
        secondBody = contact.bodyB;
    }else if (contact.bodyB == _player.physicsBody){
        secondBody = contact.bodyA;
    } else {
        NSLog(@"Collision where neither body is player. This isn't supposed to happen.");
    }
    NSLog(@"Collided with something!");
    //If secondBody is ground, apply upward impulse to player
    if (secondBody.categoryBitMask == groundCategory){
        if (!_groundHasBeenHit){
            _groundHasBeenHit = YES;
            [_player.physicsBody setVelocity:CGVectorMake(_player.physicsBody.velocity.dx,0)];
            [_player.physicsBody applyImpulse:_jumpVector];
            NSLog(@"Bounced off of ground!");
        }
    } else if (secondBody.categoryBitMask == bottomEdgeCategory){
        if (!gameOver){
        NSLog(@"Went through bottom edge!");
        //display gameOverNode
        [self removeChildrenInArray:@[_player]];
        [_camParent addChild:_gameOverNode];
        NSString *scoreString = [NSString stringWithFormat:@"Your score: %d",maxHeight-512];
        SKLabelNode *finalScoreLabel = [SKLabelNode labelNodeWithText:scoreString];
        [finalScoreLabel setFontName:@"Futura"];
        [finalScoreLabel setFontSize:20];
        [finalScoreLabel setFontColor:[SKColor whiteColor]];
        [finalScoreLabel setPosition:CGPointMake(0,0)];
        
        SKLabelNode *gameOverLabel = [SKLabelNode labelNodeWithText:@"Game over!"];
        [gameOverLabel setFontName:@"Futura"];
        [gameOverLabel setFontSize:20];
        [gameOverLabel setFontColor:[SKColor whiteColor]];
        [gameOverLabel setPosition:CGPointMake(0,50)];
        
        SKLabelNode *tryAgainLabel = [SKLabelNode labelNodeWithText:@"Tap anywhere to try again!"];
        [tryAgainLabel setFontName:@"Futura"];
        [tryAgainLabel setFontSize:20];
        [tryAgainLabel setFontColor:[SKColor whiteColor]];
        [tryAgainLabel setPosition:CGPointMake(0,-50)];
        
        [_gameOverNode addChild:tryAgainLabel];
        [_gameOverNode addChild:gameOverLabel];
        [_gameOverNode addChild:finalScoreLabel];
        
        gameOver = true;
        }
    }

    if (secondBody.categoryBitMask == leftEdgeCategory){
        [_player runAction:[SKAction moveToX:(BASE_WIDTH-50) duration:0]];
        NSLog(@"Collided with left edge.");
    } else if (secondBody.categoryBitMask == rightEdgeCategory){
        [_player runAction:[SKAction moveToX:(50) duration:0]];
        NSLog(@"Collided with right edge.");
        
    }
    if (secondBody.categoryBitMask == ballCategory){
        [_player.physicsBody setVelocity:CGVectorMake(_player.physicsBody.velocity.dx,0)];
        [_player.physicsBody applyImpulse:CGVectorMake(0,STAR_JUMP_POWER)];
        [secondBody.node removeFromParent];
    }
}



@end
